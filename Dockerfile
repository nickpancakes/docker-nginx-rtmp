FROM janeczku/alpine-kubernetes:3.3

RUN apk --no-cache add ffmpeg ca-certificates openssl pcre zlib
RUN apk --no-cache add -t builddeps build-base git wget pcre-dev openssl-dev libxslt-dev libxml2-dev
RUN mkdir -p /tmp/nginx && \
    wget http://nginx.org/download/nginx-1.10.0.tar.gz -O - | tar -zxf - -C /tmp/nginx
RUN git clone https://github.com/sergey-dryabzhinsky/nginx-rtmp-module.git /tmp/nginx/nginx-rtmp-module
RUN git clone https://github.com/openresty/headers-more-nginx-module.git /tmp/nginx/headers-more-nginx-module
RUN cd /tmp/nginx/nginx-1.10.0 && \
    ./configure \
        --prefix=/var/lib/nginx \
        --sbin-path=/usr/sbin/nginx \
        --conf-path=/etc/nginx/nginx.conf \
        --pid-path=/run/nginx/nginx.pid \
        --lock-path=/run/nginx/nginx.lock \
        --user=nginx \
        --group=nginx \
        --without-http_uwsgi_module \
        --without-http_scgi_module \
        --without-http_fastcgi_module \
        --with-ipv6 \
        --with-pcre-jit \
        --with-http_dav_module \
        --with-http_ssl_module \
        --with-http_stub_status_module \
        --with-http_gzip_static_module \
        --with-http_v2_module \
        --with-http_auth_request_module \
        --with-http_xslt_module \
        --with-http_flv_module \
        --with-http_mp4_module \
        --with-http_realip_module \
        --with-mail \
        --with-mail_ssl_module \
        --add-module=/tmp/nginx/nginx-rtmp-module \
        --add-module=/tmp/nginx/headers-more-nginx-module && \
    cd /tmp/nginx/nginx-1.10.0 && make && make install && \
    cp /tmp/nginx/nginx-rtmp-module/stat.xsl /etc/nginx && \
    adduser -D nginx

COPY nginx.conf /etc/nginx/nginx.conf
COPY rtmp.conf /etc/nginx/rtmp.conf
COPY hls.conf /etc/nginx/conf.d/hls.conf
COPY rtmp_stat.conf /etc/nginx/conf.d/rtmp_stat.conf

EXPOSE 1935 8084 8088

CMD nginx
